{
  description = "A nix development flake generator.";

  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
  };

  outputs = { self, flake-utils, nixpkgs }@inputs: flake-utils.lib.eachDefaultSystem (
    system: let
      overlays = [ ];
      pkgs = import nixpkgs { inherit system overlays; };
    in {
      devShells.default = pkgs.mkShell {
        buildInputs = with pkgs; [
          colorized-logs
          gum
          python3
        ];
        packages = with pkgs; [
        ];
        shellHook = ''
        '';
      };
    }
  );
}
