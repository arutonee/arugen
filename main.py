import gum

from languages.blank import blank
from languages.clojure import clojure
from languages.gleam import gleam
from languages.python import python
from languages.rust import rust_cargo

languages = {
    'Blank': blank,
    'Clojure': clojure,
    'Gleam': gleam,
    'Python': python,
    'Rust (Cargo)': rust_cargo,
}

if __name__ == '__main__':
    selected_language = gum.choose_one(
        'Language',
        [str(key) for key in languages.keys()],
    )

    print(languages[selected_language]())
