import base
import gum
import util


def python() -> str:
    py_version = gum.choose_one('Python Version',
                                util.query_pkgs(r'^python\d+$'))

    manage_with_nix = gum.confirm('Manage packages with Nix')

    aliases = {
        'yapf': 'yapf',
        'pyright': 'nodePackages_latest.pyright',
    }
    dev_pkgs = []
    extra_pkgs = gum.choose_many('Extra packages', list(aliases.keys()))
    dev_pkgs.extend(aliases[name] for name in extra_pkgs)

    if manage_with_nix:
        return base.base(
            lets={
                'pyenv': f'pkgs.{py_version}.withPackages (ps: with ps; [])'
            },
            build_inputs=['pyenv'],
            packages=dev_pkgs,
        )

    return base.base(
        build_inputs=[py_version],
        packages=dev_pkgs,
        shell_hooks=['echo "Don\'t forget to load the virtual environment!"'])
