import base


def clojure() -> str:
    return base.base(
        build_inputs=[
            "clojure",
            "leiningen",
        ],
        packages=[
            "babashka",
            "clojure-lsp",
        ],
    )
