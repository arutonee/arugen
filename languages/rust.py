import base
import gum


def rust_cargo() -> str:
    version = gum.choose_one('Version', ['stable', 'beta', 'nightly'])
    package_preset_options = {
        'Blank': [],
        'Bevy': [
            'alsa-lib', 'libxkbcommon', 'openssl', 'pkg-config', 'udev',
            'vulkan-loader', 'wayland', 'xorg.libX11', 'xorg.libXcursor',
            'xorg.libXi', 'xorg.libXrandr'
        ],
    }
    package_preset = gum.choose_one('Presets',
                                    list(package_preset_options.keys()))

    return base.base(
        args=['rust-overlay'],
        inputs={'rust-overlay': 'github:oxalica/rust-overlay'},
        overlays=['(import rust-overlay)'],
        build_inputs=[f'rust-bin.{version}.latest.default'] +
        package_preset_options[package_preset],
        extra_output=[
            'LD_LIBRARY_PATH = "${pkgs.lib.makeLibraryPath buildInputs}"'
        ])
