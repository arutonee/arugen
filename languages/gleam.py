import base


def gleam() -> str:
    return base.base(packages=['erlang', 'gleam', 'rebar3'])
