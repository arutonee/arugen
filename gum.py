import subprocess


def call(parts: list[str]) -> str:
    process = subprocess.Popen(parts, stdout=subprocess.PIPE, text=True)
    process.wait()
    if process.returncode == 130: raise Exception('User aborted.')
    if process.stdout == None: raise Exception('Can\'t access process stream.')
    return '\n'.join(s.strip() for s in process.stdout.readlines())


def choose_one(desc: str, args: list[str]) -> str:
    return call(['gum', 'filter', '--placeholder', desc] + args)


def choose_many(desc: str, args: list[str]) -> list[str]:
    return [
        s for s in call([
            'gum',
            'filter',
            '--no-limit',
            '--placeholder',
            desc,
        ] + args).split('\n') if s != ''
    ]


def input(desc: str) -> str:
    return call(['gum', 'input', '--placeholder', desc])


def write(desc: str) -> str:
    return call(['gum', 'write', '--placeholder', desc])


def confirm(desc: str) -> bool:
    process = subprocess.Popen(['gum', 'confirm', desc + '?'],
                               stdout=subprocess.PIPE)
    return process.wait() == 0
