from typing import Callable
import util
import gum


def base(
        *,
        args: list[str] = [],
        build_inputs: list[str] = [],
        desc: str | Callable[[], str] = lambda: gum.input('Description'),
        extra_output: list[str] = [],
        # extra_pkgs: list[str] | Callable[[], list[str]] = lambda: gum.choose_many('Extra Packages', util.get_pkgs()),
        inputs: dict[str, str] = {},
        lets: dict[str, str] = {},
        overlays: list[str] = [],
        packages: list[str] = [],
        shell_hooks: list[str] = []) -> str:
    ser_args = ''.join(f', {arg}' for arg in args)
    ser_build_inputs = ''.join(f'          {inp}\n' for inp in build_inputs)
    if callable(desc):
        ser_desc = desc()
    else:
        ser_desc: str = desc
    ser_extra_output = ''.join(f'        {s};\n' for s in extra_output)
    # if callable(extra_pkgs):
    #     real_extra_pkgs = sorted(extra_pkgs())
    # else:
    #     real_extra_pkgs = extra_pkgs
    ser_inputs = ''.join(f'    {k}.url = "{inputs[k]}";\n' for k in inputs)
    ser_packages = ''.join(f'          {p}\n' for p in packages)
    # ser_packages = ''.join(f'          {p}\n' for p in packages + real_extra_pkgs)
    ser_overlays = ''.join(f'{overlay} ' for overlay in overlays)
    ser_let = ''.join(f'      {k} = {lets[k]};\n' for k in lets)
    ser_shell_hooks = ''.join(f'        {hook}\n' for hook in shell_hooks)
    return f'''{{
  description = "{util.serialize(ser_desc)}";

  inputs = {{
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
{ser_inputs}  }};

  outputs = {{ self, flake-utils, nixpkgs{ser_args} }}@inputs: flake-utils.lib.eachDefaultSystem (
    system: let
      overlays = [ {ser_overlays}];
      pkgs = import nixpkgs {{ inherit system overlays; }};
{ser_let}    in {{
      devShells.default = pkgs.mkShell rec {{
        buildInputs = with pkgs; [
{ser_build_inputs}        ];
        packages = with pkgs; [
{ser_packages}        ];
        shellHook = ''
{ser_shell_hooks}        '';
{ser_extra_output}      }};
    }}
  );
}}'''
