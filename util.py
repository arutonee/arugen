import subprocess
import re


def serialize(s: str) -> str:
    return s.replace('\\', '\\\\').replace('\'', '\\\'').replace('\"', '\\\"')


def call(parts: list[str]) -> str:
    process = subprocess.Popen(parts, stdout=subprocess.PIPE, text=True)
    return process.communicate()[0]


def get_pkgs(sublevel: str | None = None) -> list[str]:
    extra = f'.{sublevel}' if sublevel != None else ''

    return [
        s for s in call([
            'sh',
            '-c',
            f'echo "(import <nixpkgs>{{}}).lib.mapAttrsToList (n: _: n) (import <nixpkgs>{{}}).pkgs{extra}" | nix repl 2> /dev/null | ansi2txt | tr " \\"" "\\n" | rg -v "^$"',
        ]).split('\n') if s not in ['[', ']']
    ]


def query_pkgs(regex: str, sublevel: str | None = None) -> list[str]:
    return [p for p in get_pkgs(sublevel) if re.match(regex, p)]
